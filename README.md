Read me
==============

Installation
--------------

Before usage you must have node.js with npm (node package manager) installed.

First install grunt as a console service. Run

	npm install -g grunt-cli

Then install bower . Run

	npm install -g bower

This will  make grunt and bower commands available from console.

You can read more here http://gruntjs.com/getting-started and here http://bower.io/

After grunt instalation run the following command from directory where package.json is located.

    npm install

This will install grunt, which is used for LESS compilation into CSS, EJS templates compilation and other automated tasks.

Then install required JS libraries, which are requred for application. Run

	bower install

After all this done, you are ready to develop the application. Run the following command to build the project and run livereload.

	grunt watch

This will create build directory in the project root. Open the directory by your web server (e.g. xampp). 
Grunt watch is configured so that, when JS or LESS files are changed, the page is reloaded automatically by livereload.